package org.acme.resources;

import org.acme.dto.RequestActorDTO;
import org.acme.entity.Actor;
import org.acme.repository.ActorRepository;
import org.acme.staticvalues.ContextPathConstant;
import org.acme.staticvalues.PathConstant;

import javax.inject.Inject;
import javax.ws.rs.*;
import java.util.Optional;

@Path(ContextPathConstant.API + ContextPathConstant.ACTOR)
public class ActorResource {

    @Inject
    ActorRepository actorRepository;

    @GET
    @Path(PathConstant.ACTOR_LIST)
    @Produces("application/json")
    public Iterable<Actor> findAll() {
        return actorRepository.findAll();
    }

    @POST
    @Path(PathConstant.CREATE)
    @Produces("aplication/json")
    public Actor create(RequestActorDTO param){
        Actor actor = new Actor();
        actor.setFirstName(param.getFirstName());
        actor.setLastName(param.getLastName());
        actor.setFullName(param.getFirstName()+" "+param.getLastName());
        actor.setGender(param.getGender().getName().charAt(0));
        return actorRepository.save(actor);
    }

    @PUT
    @Path(PathConstant.UPDATE)
    @Produces("aplication/json")
    public Actor updateActor(Long id, RequestActorDTO param){
        Optional<Actor> optional = actorRepository.findById(id);
        if(optional.isPresent()){
            Actor actor = optional.get();
            actor.setFirstName(param.getFirstName());
            actor.setLastName(param.getLastName());
            actor.setFullName(param.getFirstName()+" "+param.getLastName());
            actor.setGender(param.getGender().getName().charAt(0));
            return actorRepository.save(actor);
        }
        throw new IllegalArgumentException("No Actor with id " + id + " exists");
    }

    @DELETE
    @Path(PathConstant.DELETE)
    @Produces("application/json")
    public void deleteActor(Long id){
        actorRepository.deleteById(id);
    }

}
