package org.acme.staticvalues;

public class PathConstant {

    public static final String CREATE = "/create";
    public static final String UPDATE = "/update/id/{id}";
    public static final String DELETE = "/delete/{id}";
    public static final String ACTOR_LIST = "/actors";
}
