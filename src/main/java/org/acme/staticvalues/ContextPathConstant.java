package org.acme.staticvalues;

public class ContextPathConstant {

    public static final String API = "/api";
    public static final String ACTOR = "/actor";

}
